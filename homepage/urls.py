from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('skills/', views.skills, name='skills'),
    path('experiences/', views.experiences, name='experiences'),
    path('contact/', views.contact, name='contact'),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/create', views.schedule_create, name='schedule_create'),
    path('schedule/clear', views.schedule_clear, name='schedule_clear'),
    path('schedule/delete/<int:id>', views.schedule_delete, name='schedule_delete'),
    path('comingsoon/', views.comingsoon, name='comingsoon'),
]