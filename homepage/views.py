from django.shortcuts import render, redirect
from .models import Schedule
from . import forms

# Create your views here.

def home(request):
    return render(request, 'home.html')

def about(request):
    return render(request, 'about.html')

def skills(request):
    return render(request, 'skills.html')

def experiences(request):
    return render(request, 'experiences.html')

def contact(request):
    return render(request, 'contact.html')

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedules':schedules})

def schedule_create(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('schedule')
    else:
        form = forms.ScheduleForm()
    return render(request, 'schedule_create.html', {'form': form})

def schedule_clear(request):
    Schedule.objects.all().delete()
    return render(request, 'schedule.html')

def schedule_delete(request, id):
    schedule = Schedule.objects.get(id=id)
    schedule.delete()
    return redirect('schedule')

def comingsoon(request):
    return render(request, 'comingsoon.html')